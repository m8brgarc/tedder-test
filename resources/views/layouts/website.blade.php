<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/micromodal/dist/micromodal.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
    <script src="{{ asset('js/app.js') }}?v={{ time() }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}?v={{ time() }}" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        <header>
            <div class="container-fluid">
                <div class="row align-items-center justify-content-between">
                    <div class="col-8 col-sm-4 col-lg-3">
                        <a class="logo" href="/">
                            <img src="/images/best-holster_agh-01.svg" alt="Alien Gear Holsters">
                        </a>
                    </div>
                    <div class="col-auto col-lg-9">
                        <nav class="d-none d-lg-flex">
                            <ul class="main-nav">
                                @foreach(config('global.main_nav') as $item)
                                    <li><a href="{{ $item['url'] }}">{{ $item['text'] }}</a></li>
                                @endforeach
                            </ul>
                            <ul class="mini-nav">
                                @foreach(config('global.mini_nav') as $item)
                                    <li><a href="{{ $item['url'] }}"><i class="fa fa-{{ $item['icon'] }}"></i></a></li>
                                @endforeach
                            </ul>
                        </nav>
                        <button id="menu-btn" class="hamburger hamburger--collapse d-block d-lg-none" type="button">
                          <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                          </span>
                        </button>
                    </div>
                </div>
            </div>
        </header>
        <main>
            @yield('content')
        </main>
        <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col text-center">
                        &copy; {{ date('Y') }} Alien Gear Holsters. All Rights Reserved. Patents Pending.
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <aside id="mobile-menu">
        <ul class="mini-nav">
            @foreach(config('global.mini_nav') as $item)
                <li><a href="{{ $item['url'] }}"><i class="fa fa-{{ $item['icon'] }}"></i></a></li>
            @endforeach
        </ul>
        <ul class="main-nav">
            @foreach(config('global.main_nav') as $item)
                <li><a href="{{ $item['url'] }}">{{ $item['text'] }}</a></li>
            @endforeach
        </ul>
    </aside>
</body>
</html>
