@extends('layouts.website')

@section('content')
<section class="hero" style="background: url('/images/how-to-wear-the-shapeshift-iwb-holster.jpg') no-repeat center center / cover;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-8 col-xl-6 text-center">
                <h1>IWB Holsters</h1>
                <p>The most advanced, most comfortable, most modular holsters you'll ever wear.</p>
                <a href="/shop">Shop All Holsters</a>
            </div>
        </div>
    </div>
</section>
<section class="intro">
    <div class="split-img right" style="background: url('/images/how-to-wear-the-shapeshift-iwb-holster.jpg') no-repeat center center / cover;"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5">
                <span class="headline">Why Alien Gear Is The Leader</span>
                <h2>Every IWB Holster can be customized for your perfect carry position and draw.</h2>
                <p>Every Alien Gear IWB holster is designed to adjust the ride height, grip cant and retention so the wearer can dial these settings to precisely what they desire.</p>
            </div>
        </div>
    </div>
</section>
<section class="product-list">
    <form class="product-filters">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="fields">
                        <div class="field">
                            <label class="header">Filters <i class="fa fa-sliders-h d-none d-sm-inline-block"></i></label>
                            <div class="filters">
                                <label class="select">
                                    <select name="brand">
                                        <option value="Sig Sauer">Sig Sauer</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="field">
                            <label class="header">Sort By</label>
                            <label class="select">
                                <select name="sortby">
                                    <option value="weight asc">Lightest Weight</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="grid-container">
                    @for ($i = 0; $i < 6; $i++)
                        <article>
                            <ul>
                                <li><i class="fa fa-compress-arrows-alt"></i> Slim Profile</li>
                                <li><i class="fa fa-draw-polygon"></i> Multi-Position</li>
                            </ul>
                            <figure>
                                <img src="/images/most-concealable-shapeshift-iwb-holster.jpg" alt="Product Image">
                                <figcaption>
                                    <h4>Alien Gear 2 Holster Combo</h4>
                                    <div class="rating">
                                        <img src="/images/temp-stars.png"> 4.25 (28 Reviews)
                                    </div>
                                    <div class="price">$69.50</div>
                                    <ul class="actions d-none d-sm-flex">
                                        <li><button class="quick-view"><i class="fa fa-eye"></i> Quick View</button></li>
                                        <li><button class="compare"><i class="fa fa-exchange-alt"></i> Compare</button></li>
                                    </ul>
                                </figcaption>
                            </figure>
                        </article>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</section>
<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3>Features & Benefits</h3>
                <p class="headline">Premium Materials and Premium Craftsmanship</p>
            </div>
        </div>
    </div>
    <div class="feature-container">
        <div class="container">
            <div class="row justify-content-end">
                <div class="split-img left bg-image d-none d-lg-flex" style="background-color: #3b444b;">
                    <aside>
                        <i class="fa fa-cubes"></i>
                        <h4>CoolVent Neoprene</h4>
                        <p>Every IWB holster is produced with our unique CoolVent Neoprene to help keep you cool where you need it most.</p>
                    </aside>
                </div>
                <div class="col-12 col-lg-7">
                    <div class="row blocks text-center text-sm-left">
                        <div class="col-12 col-lg-6">
                            <i class="fa fa-cubes"></i>
                            <h4>CoolVent Neoprene</h4>
                            <p>With its single mounting point, the all-new IWB holster base offers a wider range of flex and improved comfort. Its design requires zero break- in time, so you can carry comfortably from the beginning.</p>
                        </div>
                        <div class="col-12 col-lg-6">
                            <i class="fa fa-layer-group"></i>
                            <h4>Enhanced Flexibility</h4>
                            <p>Using sweat-wicking technology, this perforated neoprene will keep you cool and dry.  This breathable material covers all previously exposed concealed carry holster hardware, pulling a soft breathable layer between you and your firearm</p>
                        </div>
                        <div class="col-12 col-lg-6">
                            <i class="fa fa-stream"></i>
                            <h4>Steel Core Holster Retention</h4>
                            <p>The patent-protected design gives the ShapeShift an extra layer of retention and stability to the ShapeShift AIWB, Belt Slide and IWB concealed carry holsters. Embedded between the soft neoprene backer and durable thermo elastomer surface, this strip of steel serves as a flexible backbone to the entire gun holster.</p>
                        </div>
                        <div class="col-12 col-lg-6">
                            <i class="fa fa-th"></i>
                            <h4>Ballistic Nylon</h4>
                            <p>You need a pistol holster that can withstand the daily grind. That's why we've inserted a layer of ballistic nylon into this modular concealed gun holster. This durable material reinforces the entire holster base.. Continue reading at: http:// aliengearholsters.com/</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="stripe-carousel" style="background: #3b444b;">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <span class="headline">What The Pros Say</span>
            </div>
        </div>
    </div>

    <div class="slider">
        @for ($i = 0; $i < 5; $i++)
            <div class="slide">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h3><i class="fa fa-quote-left"></i> Changing how we think of holsters forever...</h3>
                            <p>I am not aware of any other holster design company that puts this much time and effort into making a convertible modular holster that will do what the ShapeShift will do."</p>
                            <img src="http://placehold.it/200x50?text=wideopenspaces" alt="Testimonial Logo">
                        </div>
                    </div>
                </div>
            </div>
        @endfor
    </div>
</section>

<section class="featured-product">
    <div class="container">
        <div class="row justify-content-end">
            <div class="bg-media">
                <div class="video-container">
                    <div class="cover"><i class="fa fa-play-circle"></i></div>
                    {{--<video src="/images/coverr-wood-factory-1570352176014.mp4" width="100%" preload="none" controls></video>--}}
                </div>
            </div>
            <div class="col-12 col-md-6">
                <span class="headline">Featured Product</span>
                <h3>A New Standard Of IWB Concealed Carry Holster Comfort.</h3>
                <p>Inside the waistband our IWB holsters are standard for concealed carry. Alien Gear's IWB holsters are designed to conceal easily but carry comfortably, so you can carry all day without the typical pains of carrying with lesser quality holsters. We design our holsters to last for decades of use, and to be adjustable so you can set the cary position and holster retention to your exact preference.</p>
            </div>
        </div>
    </div>
</section>
@endsection
