require('jquery');
require('bootstrap');
var Slideout = require('slideout');


window.onload = function() {
    var slideout = new Slideout({
        'panel': document.getElementById('wrapper'),
        'menu': document.getElementById('mobile-menu'),
        'side': 'right'
    });

    var menuBtn = document.getElementById('menu-btn');
    menuBtn.addEventListener('click', function() {
        this.classList.toggle('is-active');
        slideout.toggle();
    });

    var slider = tns({
        container: '.stripe-carousel .slider',
        controls: false,
        navPosition: 'bottom',
        autoplayButtonOutput: false,
        viewportMax: true,
        slideBy: 'page',
        autoplay: true,
        autoplayTimeout: 8000
    });

    var imgBlock = document.querySelector('.features .bg-image aside');
    var featuredBlocks = document.querySelectorAll('.features .blocks > div');
    featuredBlocks.forEach(function(ele) {
        ele.addEventListener('mouseenter', function() {
            imgBlock.innerHTML = this.innerHTML;
        });
    })

    // var covers = document.querySelectorAll('.video-container .cover');
    // covers.forEach(function(cover) {
    //     cover.addEventListener('click',function(){
    //         this.style.display = 'none';
    //         this.nextElementSibling.play();
    //     },false);
    // });
};

