<?php

return [
    'main_nav' => [
        ['url' => '/search',    'text' => 'Search By Gun'],
        ['url' => '/shop',      'text' => 'Shop Now'],
        ['url' => '/reviews',   'text' => 'Reviews'],
        ['url' => '/blog',      'text' => 'Blog'],
        ['url' => '/videos',    'text' => 'Videos'],
        ['url' => '/help',      'text' => 'Help'],
        ['url' => '/contact',   'text' => 'Contact Us'],
    ],
    'mini_nav' => [
        ['url' => '/search',    'icon' => 'search'],
        ['url' => '/account',   'icon' => 'user'],
        ['url' => '/cart',      'icon' => 'shopping-bag'],
    ]
];